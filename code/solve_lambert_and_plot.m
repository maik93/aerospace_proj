% solve_lambert_and_plot: function description

% ------------------------------------------------------------
% Inputs:
%   dep_id - ID of departure planet
%   JD_dep - departure Julian Date
%   arr_id - ID of arrival planet
%   JD_arr - arrival Julian Date
%   is_animation - flag to check if it's an animation or a static plot
%
% Outputs:
%   r_dep_i - sun's distance of first planet at departure (vector) [km]
%   r_arr_f - sun's distance of second planet at arrival (vector) [km]
%   deltaV_dep - heliocentric delta V at departure (as Vprobe - Vplanet) (vector) [km/s]
%   deltaV_arr - heliocentric delta V at arrival (as Vprobe - Vplanet) (vector) [km/s]
%   total_deltaV - sum of norms of departure and arrival delta V [km/s]
% ------------------------------------------------------------

function [r_dep_i, r_arr_f, deltaV_dep, deltaV_arr, total_deltaV] = solve_lambert_and_plot(dep_id, JD_dep, arr_id, JD_arr, is_animation)
	global mu_sun days2seconds incl_venus_orbit

	if nargin ~= 5
		is_animation = 'static';
	end

	% draw planet without or with SOIs
	[r_dep_i, v_dep_i, r_arr_f, v_arr_f] = draw_initial_and_final_planets(dep_id, JD_dep, arr_id, JD_arr, false, is_animation);
	% [r_dep_i, v_dep_i, r_arr_f, v_arr_f] = draw_initial_and_final_planets(dep_id, JD_dep, arr_id, JD_arr, true, is_animation);

	% initial and final velocity of transfer orbit (solution of Lambert problem)
	[vi_transf, vf_transf] = glambert(mu_sun, [r_dep_i; v_dep_i], [r_arr_f; v_arr_f], (JD_arr - JD_dep) * days2seconds, 0);

	% Needed delta-V [km/s]
	deltaV_dep = vi_transf - v_dep_i;
	% deltaV_arr = v_arr_f - vf_transf;
	deltaV_arr = vf_transf - v_arr_f;
	total_deltaV = norm(deltaV_dep) + norm(deltaV_arr);

	% Transfer orbit
	[a_transf, h_transf, e_transf, RA_transf, incl_transf, w_transf, ~, EA_transf_dep] = ...
		state_vector_2_orbit_elements(r_dep_i, vi_transf); % [a, h, e, RA, incl, w, TA, EA]
	[~, ~, ~, ~, ~, ~, ~, EA_transf_arr] = state_vector_2_orbit_elements(r_arr_f, vf_transf);
	draw_ellipse(e_transf, a_transf, RA_transf, incl_transf, w_transf, [EA_transf_dep; EA_transf_arr]);

	if strcmpi(is_animation, 'animate')
		camorbit(0,-incl_venus_orbit*180/pi)
		animate_planets(dep_id, arr_id, JD_dep, JD_arr, a_transf, h_transf, e_transf, RA_transf, incl_transf, w_transf, EA_transf_dep);
	end

	% new_empty_figure('Escape velocity vectors');
	% draw_vector([0, 0, 0], v_dep_i, rgb('DarkTurquoise'));
	% draw_vector([0, 0, 0], vi_transf, rgb('Orange'));
	% draw_vector(v_dep_i, deltaV_dep, 'red');

	% new_empty_figure('Capture velocity vectors');
	% draw_vector([0, 0, 0], v_arr_f, rgb('DarkViolet'));
	% draw_vector([0, 0, 0], vf_transf, rgb('Orange'));
	% draw_vector(v_arr_f, deltaV_arr, 'red');