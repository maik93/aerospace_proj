% solve_hooman_and_plot: compute and draw Hohmann transfer between two given planet

% ------------------------------------------------------------
% Inputs:
%   dep_id - ID of departure planet
%   JD_dep - departure Julian Date
%   arr_id - ID of arrival planet
%   JD_arr - arrival Julian Date
%   planar_correction - flag to enable/disable non-planarity correction
%   is_animation - flag to check if it's an animation or a static plot
%
% Outputs:
%   deltaV_dep - heliocentric delta V at departure (as Vprobe - Vplanet) (vector) [km/s]
%   deltaV_arr - heliocentric delta V at arrival (as Vprobe - Vplanet) (vector) [km/s]
%   total_deltaV - sum of norms of departure and arrival delta V [km/s]
% ------------------------------------------------------------

function [deltaV_dep, deltaV_arr, total_deltaV] = solve_hooman_and_plot(dep_id, JD_dep, arr_id, JD_arr, planar_correction, is_animation)
	global mu_sun days2seconds seconds2days incl_venus_orbit

	if nargin ~= 6
		is_animation = 'static';
	end

	[r_dep_i, v_dep_i, r_arr_f, v_arr_f] = draw_initial_and_final_planets(dep_id, JD_dep, arr_id, JD_arr, false, is_animation);

	% perihelion velocity of transfer orbit
	r_perihelion = norm(r_dep_i);
	r_aphelion = norm(r_arr_f);
	% vp_norm = 2 * sqrt((mu_sun * r_aphelion) / (r_perihelion * (r_perihelion + r_aphelion)));
	% vp_norm = 2 * sqrt(mu_sun * (2*r_perihelion + r_aphelion) / (r_perihelion * (r_perihelion + r_aphelion)));

	vp_norm = sqrt(2 * mu_sun * r_aphelion / (r_perihelion * (r_perihelion + r_aphelion)));
	v_perihelion = vp_norm * v_dep_i / norm(v_dep_i);
	v_aphelion = sqrt(2 * mu_sun * r_perihelion / (r_aphelion * (r_perihelion + r_aphelion)));

	% Needed delta-V [km/s]
	deltaV_dep = v_perihelion - v_dep_i;
	deltaV_arr = v_aphelion - v_arr_f;
	total_deltaV = norm(deltaV_dep) + norm(deltaV_arr);

	% Transfer orbit
	[a_transf, h_transf, e_transf, RA_transf, incl_transf, w_transf, ~, EA_transf_dep, node_axis, H_transf] = ...
		state_vector_2_orbit_elements(r_dep_i, v_perihelion); % [a, h, e, RA, incl, w, TA, EA]

	% Classical solution ignoring inclination differences between Earth and Venus
	% if nargin ~= 5 || (nargin == 5 && planar_correction ~= true)

	EA_transf_arr = EA_transf_dep + pi;
	draw_ellipse(e_transf, a_transf, RA_transf, incl_transf, w_transf, [EA_transf_dep; EA_transf_arr]);

	% else % Correction of non cooplanarity of planet orbits
	if nargin == 5 && strcmpi(planar_correction, 'corrected')

		% The transfer orbit is subdivided in two, with an inpulse when we are on node axis

		% Encounter with node axis
		node_omega = pi - w_transf;
		node_point = h_transf^2 / (mu_sun * (1 + e_transf * cos(node_omega))) * -node_axis;
		node_point_norm = norm(node_point);
		scatter3(node_point(1), node_point(2), node_point(3), 21, 'black');

		% First part of transfer orbit
		% draw_ellipse(e_transf, a_transf, RA_transf, incl_transf, w_transf, [EA_transf_dep; node_omega]);

		% Velocity in node point
		v_node_norm = sqrt(2 * mu_sun / node_point_norm - mu_sun / a_transf);
		dir_tangent = cross(H_transf, node_point);
		dir_tangent = dir_tangent / norm(dir_tangent);
		v_tangent_norm = h_transf / node_point_norm;
		v_node = v_tangent_norm * dir_tangent + sqrt(v_node_norm^2 - v_tangent_norm^2) * node_point / node_point_norm;

		% Eccentric Anomaly there
		[~, ~, ~, ~, ~, ~, ~, EA_transf_node] = ...
			state_vector_2_orbit_elements(node_point, v_node);

		% Days from departure
		tau = sqrt(a_transf^3 / mu_sun) * (EA_transf_node - e_transf * sin(EA_transf_node)) * seconds2days;

		% Lambert solution from node point to Earth
		[v_node_2, vf_transf] = glambert(mu_sun, [node_point; v_node], [r_arr_f; v_arr_f], (JD_arr - JD_dep - tau) * days2seconds, 0);

		% Second part of transfer orbit
		[a_transf_2, ~, e_transf_2, RA_transf_2, incl_transf_2, w_transf_2, ~, EA_transf_node_2] = ...
			state_vector_2_orbit_elements(node_point, v_node_2);
		[~, ~, ~, ~, ~, ~, ~, EA_transf_arr] = state_vector_2_orbit_elements(r_arr_f, vf_transf);
		draw_ellipse(e_transf_2, a_transf_2, RA_transf_2, incl_transf_2, w_transf_2, [EA_transf_node_2; 2*pi + EA_transf_arr]);

		% additional delta-V requested for this correction
		additional_deltaV_manuver = norm(v_node_2 - v_node)

		deltaV_arr = vf_transf - v_arr_f;
	else
		if strcmpi(is_animation, 'animate')
			camorbit(0,-incl_venus_orbit*180/pi)
			animate_planets(dep_id, arr_id, JD_dep, JD_arr, a_transf, h_transf, e_transf, RA_transf, incl_transf, w_transf, EA_transf_dep);
		end
	end