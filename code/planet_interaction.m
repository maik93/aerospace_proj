% planet_interaction: compute and draw SOI interactions (escapes, captures or flybys)

% ------------------------------------------------------------
% Inputs:
%   planet_id
%   r_periapsis - radius at periapsis (vector) [km]
%   JD_dep - departure Julian Date
%   v_inf - escape velocity (vector) [km/s]
%   interaction_type - type of trajectory -> 'escape'/'capture'/'flyby'
%
% Outputs:
%   periapsis - point of periapsis (vector) [km]
%   v_periapsis_versor - velocity versor at periapsis (vector) [km/s]
%   needed_impulse_parking - delta V needed to park at periapsis quote [km/s]
%   v_inf_exit_versor - escape velocity versor (vector) [km/s]
% ------------------------------------------------------------

function [periapsis, v_periapsis_versor, needed_impulse_parking, v_inf_exit_versor] = planet_interaction(planet_id, r_periapsis, JD_dep, v_inf, interaction_type)
	global r_earth r_venus mu_earth mu_venus

	if planet_id == 3
		mu_planet = mu_earth;
		planet_col = rgb('DarkTurquoise');
	elseif planet_id == 2
		mu_planet = mu_venus;
		planet_col = rgb('DarkViolet');
	end

	% Parking orbit velocity
	v_park = sqrt(mu_planet / r_periapsis);

	% Planet heliocentric velocity at departure time
	[R_planet, V_planet] = p2000(11, planet_id, JD_dep);

	% needed delta-V
	v_inf_norm = norm(v_inf);
	needed_impulse_parking = v_park * (sqrt(2 + (v_inf_norm / v_park)^2) - 1);

	% draw_soi(planet_id, R_planet, true);
	draw_sphere([0, 0, 0], 200, planet_col, 1); % Planet (NOT in scale)
	[periapsis, v_periapsis_versor, v_inf_exit_versor] = ...
		draw_hyperbola(mu_planet, r_periapsis, V_planet, v_inf, interaction_type); % Escaping hyperbola
	draw_vector([0, 0, 0], 2e4 * -R_planet / norm(R_planet), rgb('Orange')); % To-the-Sun
	draw_vector([0, 0, 0], 2e4 * V_planet / norm(V_planet), planet_col); % Planet velocity

	draw_vector([0, 0, 0], 1e4 * v_inf / v_inf_norm, rgb('Red')); % deltaV