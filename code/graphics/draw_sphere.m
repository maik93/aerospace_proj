% Draw a circle at coordinate pos3D (vector) of radius 'r' and color 'col'

% ------------------------------------------------------------
% Inputs:
%   pos3D - coordinate to draw the sphere (vector)
%   r - sphere radious (I think they're pixels, not plot units)
%   col - color (rbg or textual)
%   filled - flag that will make the sphere filled or not
% ------------------------------------------------------------

function [fig] = draw_sphere(pos3D, r, col, filled)
	if filled == 1
		fig = scatter3(pos3D(1), pos3D(2), pos3D(3), r, col, 'filled');
	else
		fig = scatter3(pos3D(1), pos3D(2), pos3D(3), r, col);
	end

	% r = r * ones(20, 20);
	% [th, phi] = meshgrid(linspace(0, 2*pi, 20), linspace(-pi, pi, 20));
	% [x,y,z] = sph2cart(th, phi, r);
	% x = x + pos3D(1);
	% y = y + pos3D(2);
	% z = z + pos3D(3);
	% surface(x, y, z, 'FaceColor', 'none', 'EdgeColor', col)

end