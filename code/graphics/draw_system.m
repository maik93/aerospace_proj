% Draw Sun and Venus and Earth orbits

% ------------------------------------------------------------
% Inputs:
%   R_1 - sun's distance of the first planet (vector) [km]
%   V_1 - heliocentric velocity of the first planet (vector) [km/s]
%   R_2 - sun's distance of the second planet (vector) [km]
%   V_2 - heliocentric velocity of the second planet (vector) [km/s]
%   id_1 - ID of first planet
% ------------------------------------------------------------

function [] = draw_system(R_1, V_1, R_2, V_2, id_1)
	if id_1 == 3 % Earth
		col_1 = rgb('DarkTurquoise');
		col_2 = rgb('DarkViolet');
	else % Venus
		col_1 = rgb('DarkViolet');
		col_2 = rgb('DarkTurquoise');
	end

	draw_sphere([0,0,0], 100, rgb('Orange'), 1); % Sun
	% hold on
	% axis 'equal'

	% Earth orbit
	[a_1, ~, e_1, RA_1, incl_1, w_1, ~, ~] = ...
		state_vector_2_orbit_elements(R_1, V_1); % [a, h, e, RA, incl, w, TA, EA]
	draw_ellipse(e_1, a_1, RA_1, incl_1, w_1, [-pi; pi], col_1);

	% Venus orbit
	[a_2, ~, e_2, RA_2, incl_2, w_2, ~, ~] = ...
		state_vector_2_orbit_elements(R_2, V_2); % [a, h, e, RA, incl, w, TA, EA]
	draw_ellipse(e_2, a_2, RA_2, incl_2, w_2, [-pi; pi], col_2);