% draw_ellipse: draw an ellipse with focus on the origin

% ------------------------------------------------------------
% Inputs:
%   e - eccentricity
%   a - semimajor axis [km]
%   RA - right ascension of the ascending node (a.k.a. Omega) [rad]
%   incl - inclination of the orbit [rad]
%   omega - argument of periapsis [rad]
%   EA_window - interval of eccentric anomaly to draw [rad]
% ------------------------------------------------------------

function [] = draw_ellipse(e, a, RA, incl, omega, EA_window, col)
	if nargin == 5
		EA_window = [-pi; pi];
	end

	if e >= 1 - 1e-5 % || EA_window(1) > EA_window(2)
		return
	end

	if EA_window(1) > EA_window(2)
		temp = EA_window(1);
		EA_window(1) = EA_window(2);
		EA_window(2) = temp;
	end

	focus = a * e;
	b = a * sqrt(1 - e^2);

	EA = EA_window(1):0.01:EA_window(2);

	% planar ellipse
	x = a * cos(EA) - focus;
	y = b * sin(EA);
	z = zeros(1, length(EA));
	planar_ellipse = [x; y; z];

	% rot_mat = transpose(angle2dcm(RA, incl, omega, 'ZXZ'));
	rotZXZ = matrix_rotation(RA, 'Z') *  matrix_rotation(incl, 'X') * matrix_rotation(omega, 'Z');
	ellipse = rotZXZ * planar_ellipse;
	if nargin == 7
		plot3(ellipse(1,:), ellipse(2,:), ellipse(3,:), 'color', col);
	else
		plot3(ellipse(1,:), ellipse(2,:), ellipse(3,:));
	end
	hold on

	% Periapsis TODO: flag draw_periapsis
	% periapsis = rotZXZ * [a - focus; 0; 0];
	% scatter3(periapsis(1), periapsis(2), periapsis(3), 21, 'black', 'filled')

	% Ascending point
	% RA_point = rotZXZ * [a * cos(omega) - focus; - a * sin(omega); 0];
	% scatter3(RA_point(1), RA_point(2), RA_point(3), 21, 'black');

	% Descending point
	% RD_point = rotZXZ * [a * cos(pi + omega) - focus; - a * sin(pi + omega); 0];
	% scatter3(RD_point(1), RD_point(2), RD_point(3), 21, 'black');

	% TODO: be more accurate on node localization (look solve_hohmann_and_plot:37)