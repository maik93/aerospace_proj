% draw_hyperbola: draw an hyperbola with focus on the origin

% ------------------------------------------------------------
% Inputs:
%   mu_planet - gravitational parameter [km^3/s^2]
%   r_periapsis - radius at periapsis (vector) [km]
%   v_planet - velocity of the planet [km/s]
%   v_inf - escape velocity (vector) [km/s]
%   interaction_type - type of trajectory -> 'escape'/'capture'/'flyby'
%   length_limit - maximum length of the drawn hyperbola
%
% Outputs:
%   periapsis - point of periapsis (vector) [km]
%   v_periapsis_versor - velocity versor at periapsis (vector) [km/s]
%   v_inf_exit_versor - escape velocity versor (vector) [km/s]
% ------------------------------------------------------------

function [periapsis, v_periapsis_versor, v_inf_exit_versor] = draw_hyperbola(mu_planet, r_periapsis, v_planet, v_inf, interaction_type, length_limit)
	if nargin ~= 6
		length_limit = 5 * r_periapsis;
	end

	if strcmpi(interaction_type, 'capture') || strcmpi(interaction_type, 'flyby')
		v_inf = -v_inf;
	end

	a = - mu_planet / norm(v_inf)^2;
	focus = a - r_periapsis;
	e = focus / a;
	b = abs(a) * sqrt(e^2 - 1);

	% Escape angle
	beta_angle = acos(1 / e);

	% turn angle
	delta = pi - 2 * beta_angle;

	v_planet_angle = acos(v_planet(1) / norm(v_planet));
	if v_planet(1) < 0
		v_planet_angle = 2*pi - v_planet_angle;
	end

	deltaV_xy = [v_inf(1); v_inf(2); 0];
	z_rotation_angle = - acos(deltaV_xy(1) / norm(deltaV_xy));
	y_rotation_angle = acos(dot(v_inf, deltaV_xy) / (norm(v_inf) * norm(deltaV_xy)));

	if strcmpi(interaction_type, 'escape')
		theta_inf = (pi + delta) / 2;
	elseif strcmpi(interaction_type, 'capture') || strcmpi(interaction_type, 'flyby')
		z_rotation_angle = -z_rotation_angle;
		y_rotation_angle = -y_rotation_angle;
		theta_inf =  pi + (pi - delta) / 2;
		b = -b; % invert y points
	else
		fprintf('you have to specify interaction_type: escape, capture or flyby.')
		return
	end

	if strcmpi(interaction_type, 'flyby')
		theta = -pi/2:0.01:pi/2;
	else
		theta = 0:0.01:pi/2;
	end

	x = a * cosh(theta) - focus;
	y = b * sinh(theta);

	% limit hyperbola length
	if length_limit ~= -1
		for i = 1:length(theta)
			if abs(b * sinh(theta(i))) < length_limit
				x(i) = a * cosh(theta(i)) - focus;
				y(i) = b * sinh(theta(i));
			end
		end
	else
		x = a * cosh(theta) - focus;
		y = b * sinh(theta);
	end

	rotZYZ = matrix_rotation(z_rotation_angle, 'Z') *  matrix_rotation(y_rotation_angle, 'Y') * matrix_rotation(-theta_inf, 'Z');
	% rotZYZ = [1 0 0; 0 1 0; 0 0 1];
	r = rotZYZ * [x; y; zeros(1, length(theta))];

	plot3(r(1,1:i), r(2,1:i), r(3,1:i));
	hold on

	% Periapsis
	periapsis = rotZYZ * [a - focus; 0; 0];
	scatter3(periapsis(1), periapsis(2), periapsis(3), 21, 'black', 'filled')

	% Versor of velocity at periapsis
	v_periapsis_versor = rotZYZ * [0; 1; 0];
	% draw_vector(periapsis, 5e3 * v_periapsis_versor);

	if strcmpi(interaction_type, 'flyby')
		v_inf_exit_versor = rotZYZ * matrix_rotation(pi - beta_angle, 'Z') * [1; 0; 0];
		draw_vector([0, 0, 0], 1e4 * v_inf_exit_versor, 'red');

		deltaV_flyby_versor = (v_inf / norm(v_inf) + v_inf_exit_versor) / norm(v_inf / norm(v_inf) + v_inf_exit_versor);
		draw_vector([0, 0, 0], 1e4 * deltaV_flyby_versor, 'blue');
	else
		v_inf_exit_versor = [];
	end