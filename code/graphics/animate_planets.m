% animate_planets: animate planets and and the probe (on its transfer orbit) in a given period

% ------------------------------------------------------------
% Inputs:
%   dep_id - ID of departure planet
%   arr_id - ID of arrival planet
%   JD_dep - departure Julian Date
%   JD_arr - arrival Julian Date
%   a_transf - semimajor axis [km]
%   h_transf - angular momentum [km^2/s]
%   e_transf - eccentricity
%   RA_transf - right ascension of the ascending node [rad]
%   incl_transf - inclination of the orbit [rad]
%   w_transf - argument of perigee [rad]
%   EA_transf_dep - eccentric anomaly at departure [rad]
% ------------------------------------------------------------

function [] = animate_planets(dep_id, arr_id, JD_dep, JD_arr, a_transf, h_transf, e_transf, RA_transf, incl_transf, w_transf, EA_transf_dep)
	global mu_sun days2seconds store_animation

	if store_animation == true
		mkdir src/
	end

	if dep_id == 3 % Earth
		col_1 = rgb('DarkTurquoise');
		col_2 = rgb('DarkViolet');
	else % Venus
		col_1 = rgb('DarkViolet');
		col_2 = rgb('DarkTurquoise');
	end

	k = 0;
	for JD = JD_dep:JD_arr
		[r_1, ~] = p2000(11, dep_id, JD);
		[r_2, ~] = p2000(11, arr_id, JD);

		p1 = draw_sphere(r_1, 50, col_1, 1);
		p2 = draw_sphere(r_2, 40, col_2, 1);

		M = sqrt(mu_sun / a_transf^3) * (JD - JD_dep) * days2seconds;
		EA = EA_transf_dep + kepler_E(e_transf, M);
		TA = 2 * atan(sqrt((1 + e_transf) / (1 - e_transf)) * tan(EA / 2));

		% EA = EA_transf_dep + sqrt(mu_sun / a_transf^3) * (JD - JD_dep) * days2seconds;
		% TA = 2 * atan(sqrt((1 + e_transf) / (1 - e_transf)) * tan(EA / 2));

		[r_3, ~] = orbit_elements_2_state_vector(h_transf, e_transf, RA_transf, incl_transf, w_transf, TA);
		p3 = draw_sphere(r_3, 30, rgb('Orange'), 1);

		pause(1/25); % 25 fps
		if store_animation == true
			eval(['print -dpng src/frame',num2str(k,'%03d'),'.png']);
			k = k + 1;
		end

		if JD ~= JD_arr
			delete(p1);
			delete(p2);
			delete(p3);
		end
	end