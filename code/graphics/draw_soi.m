% draw_soi: plot a wireframe sphere

% ------------------------------------------------------------
% Inputs:
%   id_planet - ID of the planet whom we are interested
%   R_planet - sun's distance of the planet (vector) [km]
%   drawAtOrigin - flag that switch sphere center from origin to R_planet
% ------------------------------------------------------------

function [] = draw_soi(id_planet, R_planet, drawAtOrigin)
	global mass_sun mass_earth mass_venus

	if id_planet == 2 % Venus
		r_soi = norm(R_planet) * (mass_venus / mass_sun)^(2/5);
	elseif id_planet == 3 % Earth
		r_soi = norm(R_planet) * (mass_earth / mass_sun)^(2/5);
	end

	r = r_soi * ones(20, 20);
	[th, phi] = meshgrid(linspace(0, 2*pi, 20), linspace(-pi, pi, 20));
	[x,y,z] = sph2cart(th, phi, r);

	if nargin ~= 3 || drawAtOrigin ~= true
		x = x + R_planet(1);
		y = y + R_planet(2);
		z = z + R_planet(3);
	end

	surface(x, y, z, 'FaceColor', 'none', 'EdgeColor', 'black')