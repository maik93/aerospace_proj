% draw_initial_and_final_planets: plot initial (full circles) and final (empty circles) position of two given planets

% ------------------------------------------------------------
% Inputs:
%   dep_id - ID of departure planet
%   JD_dep - departure Julian Date
%   arr_id - ID of arrival planet
%   JD_arr - arrival Julian Date
%   drawSOI - flag to enable/disable SOI plot
%   is_animation - flag to check if it's an animation or a static plot
%
% Outputs:
%   r_dep_i - sun's distance of first planet at departure (vector) [km]
%   v_dep_i - velocity of first planet at departure (vector) [km]
%   r_arr_f - sun's distance of second planet at arrival (vector) [km]
%   v_arr_f - velocity of second planet at arrival (vector) [km]
% ------------------------------------------------------------

function [r_dep_i, v_dep_i, r_arr_f, v_arr_f] = draw_initial_and_final_planets(dep_id, JD_dep, arr_id, JD_arr, drawSOI, is_animation)
	if dep_id == 2 % Venus
		dep_col = rgb('DarkViolet');
		arr_col = rgb('DarkTurquoise');
	else % Earth
		dep_col = rgb('DarkTurquoise');
		arr_col = rgb('DarkViolet');
	end

	% initial state vector
	[r_dep_i, v_dep_i] = p2000(11, dep_id, JD_dep);
	[r_arr_i, ~] = p2000(11, arr_id, JD_dep);

	% final state vector
	[r_arr_f, v_arr_f] = p2000(11, arr_id, JD_arr);
	[r_dep_f, ~] = p2000(11, dep_id, JD_arr);

	draw_system(r_dep_i, v_dep_i, r_arr_f, v_arr_f, dep_id);

	if nargin ~= 6 || strcmpi(is_animation, 'animate') == false
		% Initial planet positions (filled circles)
		draw_sphere(r_dep_i, 50, dep_col, 1);
		draw_sphere(r_arr_i, 50, arr_col, 1);

		% Final planet positions (empty circles)
		draw_sphere(r_dep_f, 50, dep_col, 0);
		draw_sphere(r_arr_f, 50, arr_col, 0);
	end

	if nargin >= 5 && drawSOI == true
		draw_soi(dep_id, r_dep_i);
		draw_soi(arr_id, r_arr_f);
	end