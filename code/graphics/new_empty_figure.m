% new_empty_figure

function [] = new_empty_figure(fig_title)
	figure('name', fig_title);
	hold on
	axis equal