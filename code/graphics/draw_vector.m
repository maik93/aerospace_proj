% draw_vector: draw a 3D vector in a given point

% ------------------------------------------------------------
% Inputs:
%   origin - position of the vector tail (vector)
%   vector - length of the vector (vector)
%   col - color (rbg or textual)
% ------------------------------------------------------------

function [] = draw_vector(origin, vector, col)
	if nargin == 3
		quiver3(origin(1), origin(2), origin(3), vector(1), vector(2), vector(3), 1, 'color', col);
	else
		quiver3(origin(1), origin(2), origin(3), vector(1), vector(2), vector(3), 1);
	end