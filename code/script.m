% ------------------------------- MAIN FILE -------------------------------


% Preliminary mission analysis
% create_porkchop

% initialize global variables
init

global r_earth r_venus mu_earth mu_venus incl_venus_orbit

% -------------------------------------------------------------------------
% ------------------------------- DEPARTURE -------------------------------
% -------------------------------------------------------------------------
JD_dep_earth = julian(7, 31, 2018);
JD_encounter_venus = julian(12, 2, 2018);

new_empty_figure('Heliocentric departure');
[R_earth_i, R_venus_encounter, deltaV_earth_escape, deltaV_venus_encounter, total_deltaV_departure] = ...
	solve_lambert_and_plot(3, JD_dep_earth, 2, JD_encounter_venus);
% camorbit(0,-incl_venus_orbit*180/pi) % camera inclination used for report's plots

% new_empty_figure('Heliocentric departure animation');
% [R_earth_i, R_venus_encounter, deltaV_earth_escape, deltaV_venus_encounter, total_deltaV_departure] = ...
% 	solve_lambert_and_plot(3, JD_dep_earth, 2, JD_encounter_venus, 'animate');

% --------------------------- escape from Earth ---------------------------

parking_earth_altitude = 200; % [km]

r_parking_earth = r_earth + parking_earth_altitude;

new_empty_figure('Earth SOI escape');
[~, ~, needed_impulse_escape_earth] = ...
	planet_interaction(3, r_parking_earth, JD_dep_earth, deltaV_earth_escape, 'escape');

% initial circular equatorial orbit, no need to change inclination
draw_ellipse(0, r_parking_earth, 0, 0, 0);

% ------------------------- Fly-by arround Venus --------------------------

[choosen_r_periapsis, post_flyby_period] = ...
	evaluate_flyby_periapsis(deltaV_venus_encounter, JD_encounter_venus);
% choosen_r_periapsis is evaluated 20552
% post_flyby_period is evaluated 224.2701 (real is 224.7010)

% Aiming radius
aim_rad_flyby_venus = ...
	choosen_r_periapsis * sqrt(1 + 2 * mu_venus / (choosen_r_periapsis * norm(deltaV_venus_encounter)^2))

new_empty_figure('Venus fly-by');
[~, ~, ~, v_inf_exit_versor] = ...
	planet_interaction(2, choosen_r_periapsis, JD_encounter_venus, deltaV_venus_encounter, 'flyby');

v_inf_exit_flyby = norm(deltaV_venus_encounter) * v_inf_exit_versor;

% Reason to choose fly-by on Venus
% deltaV_max_earth = 2 * norm(deltaV_earth_escape) / (1 + r_earth * norm(deltaV_earth_escape)^2 / mu_earth)
% deltaV_max_venus = 2 * norm(deltaV_venus_encounter) / (1 + r_venus * norm(deltaV_venus_encounter)^2 / mu_venus)
% deltaV_flyby = norm(v_inf_exit_flyby - deltaV_venus_encounter)

[~, v_venus_encounter] = p2000(11, 2, JD_encounter_venus);
new_V_heliocentric = v_venus_encounter + v_inf_exit_flyby;

new_empty_figure('Post fly-by');
solve_lambert_and_plot(3, JD_dep_earth, 2, JD_encounter_venus);

% Post fly-by orbit
[a_post_flyby, ~, e_post_flyby, RA_post_flyby, incl_post_flyby, w_post_flyby, ~, ~] = ...
	state_vector_2_orbit_elements(R_venus_encounter, new_V_heliocentric);
draw_ellipse(e_post_flyby, a_post_flyby, RA_post_flyby, incl_post_flyby, w_post_flyby, [-pi; pi], rgb('Black'));

% Final Venus entry position
[r_venus_final, v_venus_final] = p2000(11, 2, JD_encounter_venus + post_flyby_period);
scatter3(r_venus_final(1), r_venus_final(2), r_venus_final(3), 50, rgb('Black'), 'x');

% --------------------------- capture in Venus ----------------------------

JD_arr_venus = JD_encounter_venus + post_flyby_period;

parking_venus_altitude = 500; % [km]

r_parking_venus = r_venus + parking_venus_altitude;

deltaV_venus_capture = new_V_heliocentric - v_venus_final;

% Aiming radius
aim_rad_capture_venus = ...
	r_parking_venus * sqrt(1 + 2 * mu_venus / (r_parking_venus * norm(deltaV_venus_capture)^2))

new_empty_figure('Venus SOI capture');
[periapsis_cap_venus, v_periapsis_cap_venus_versor, needed_impulse_parking_venus] = ...
	planet_interaction(2, r_parking_venus, JD_arr_venus, deltaV_venus_capture, 'capture');

% Capture orbit
[~, ~, ~, RA_hyp_cap_venus, incl_hyp_cap_venus, w_hyp_cap_venus] = ...
	state_vector_2_orbit_elements(periapsis_cap_venus, v_periapsis_cap_venus_versor);
draw_ellipse(0, r_parking_venus, RA_hyp_cap_venus, incl_hyp_cap_venus, w_hyp_cap_venus);

% Final circular polar orbit
draw_ellipse(0, r_parking_venus, 0, incl_venus_orbit + pi/2, 0);

deltaV_change_orbit_venus_capture = 2 * sqrt(mu_venus / r_parking_venus) * sin((incl_venus_orbit + pi/2 - incl_hyp_cap_venus) / 2)

% -------------------------------------------------------------------------
% -------------------------------- RETURN ---------------------------------
% -------------------------------------------------------------------------

% Departure at least 1 month after arrive
JD0 = JD_arr_venus + 30;

[t_wait, t_transf] = find_times_for_hohmann(JD0);

% Departure time from Venus
JD_dep_venus = JD0 + t_wait;

% Arrival time in Earth
JD_arr_earth = JD_dep_venus + t_transf;

% look at Lambert exact solution!
% new_empty_figure('Exact Lambert solution');
% solve_lambert_and_plot(2, JD_dep_venus, 3, JD_arr_earth);

% Hohmann with planar assumption
% new_empty_figure('Heliocentric return');
% [deltaV_venus_escape, deltaV_earth_capture, total_deltaV_return] = ...
% 	solve_hohmann_and_plot(2, JD_dep_venus, 3, JD_arr_earth);

% Hohmann with non-planarity correction
new_empty_figure('Heliocentric return');
[deltaV_venus_escape, deltaV_earth_capture, total_deltaV_return] = ...
	solve_hohmann_and_plot(2, JD_dep_venus, 3, JD_arr_earth, 'corrected');

% new_empty_figure('Heliocentric return animation');
% [deltaV_venus_escape, deltaV_earth_capture, total_deltaV_return] = ...
% 	solve_hohmann_and_plot(2, JD_dep_venus, 3, JD_arr_earth, 'planar', 'animate');

% --------------------------- escape from Venus ---------------------------

new_empty_figure('Venus SOI escape');
[periapsis_esc_venus, v_periapsis_esc_venus_versor, needed_impulse_escape_venus] = ...
	planet_interaction(2, r_parking_venus, JD_dep_venus, deltaV_venus_escape, 'escape');

% Initial circular polar orbit
draw_ellipse(0, r_parking_venus, 0, incl_venus_orbit + pi/2, 0);

% Departure orbit
[~, ~, ~, RA_hyp_esc_venus, incl_hyp_esc_venus, w_hyp_esc_venus] = ...
	state_vector_2_orbit_elements(periapsis_esc_venus, v_periapsis_esc_venus_versor);
draw_ellipse(0, r_parking_venus, RA_hyp_esc_venus, incl_hyp_esc_venus, w_hyp_esc_venus);

deltaV_change_orbit_venus_escape = 2 * sqrt(mu_venus / r_parking_venus) * sin((incl_venus_orbit + pi/2 - incl_hyp_esc_venus) / 2)

% --------------------------- capture in Earth ----------------------------

% Aiming radius
aim_rad_capture_earth = ...
	r_parking_earth * sqrt(1 + 2 * mu_earth / (r_parking_earth * norm(deltaV_earth_capture)^2))

new_empty_figure('Earth SOI capture');
[periapsis_cap_earth, v_periapsis_cap_earth_versor, needed_impulse_parking_earth] = ...
	planet_interaction(3, r_parking_earth, JD_arr_earth, deltaV_earth_capture, 'capture');

% Capture orbit
[~, ~, ~, RA_hyp_cap_earth, incl_hyp_cap_earth, w_hyp_cap_earth] = ...
	state_vector_2_orbit_elements(periapsis_cap_earth, v_periapsis_cap_earth_versor);
draw_ellipse(0, r_parking_earth, RA_hyp_cap_earth, incl_hyp_cap_earth, w_hyp_cap_earth);

% Final circular equatorial orbit
draw_ellipse(0, r_parking_earth, 0, 0, 0);

deltaV_change_orbit_earth_capture = 2 * sqrt(mu_earth / r_parking_venus) * sin(incl_hyp_cap_earth / 2)
