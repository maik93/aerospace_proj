% matrix_rotation: rotation matrix arround X, Y or Z axis

% ------------------------------------------------------------
% Inputs:
%   alpha - angle to rotate [deg]
%   axis - 'X', 'Y' or 'Z'
% ------------------------------------------------------------

function [mat] = matrix_rotation(alpha, axis)
	if strcmpi(axis, 'Z')
		mat = [cos(alpha) -sin(alpha) 0; sin(alpha) cos(alpha) 0; 0 0 1];

	elseif strcmpi(axis, 'Y')
		mat = [cos(alpha) 0 sin(alpha); 0 1 0; -sin(alpha) 0 cos(alpha)];

	elseif strcmpi(axis, 'X')
		mat = [1 0 0; 0 cos(alpha) -sin(alpha); 0 sin(alpha) cos(alpha)];

	else
		mat = [1 0 0; 0 1 0; 0 0 1];
	end
