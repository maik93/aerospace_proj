% Edited version of 'coe_from_sv.m' (algoritm 4.1, Appendix D) from 'Curtins - Orbital mechanics for engineering students'

% ------------------------------------------------------------
% This function computes the classical orbital elements [coe]
% from the state vector (R,V) using Algorithm 4.1.
%
% Inputs:
%   R - position vector in the geocentric equatorial frame [km]
%   V - velocity vector in the geocentric equatorial frame [km]
%
% Internal variables:
%   mu_sun - gravitational parameter [km^3/s^2]
%   eps - a small number below which the eccentricity is considered to be zero
%   r, v - the magnitudes of R and V
%   vr - radial velocity component [km/s]
%   N - the node line vector [km^2/s]
%   n - the magnitude of N
%   cp - cross product of N and R
%   E - eccentricity vector
%
% Outputs:
%   a - semimajor axis [km]
%   h - the magnitude of H [km^2/s]
%   e - eccentricity (magnitude of E)
%   RA - right ascension of the ascending node (a.k.a. Omega) [rad]
%   incl - inclination of the orbit [rad]
%   w - argument of perigee (a.k.a. omega) [rad]
%   TA - true anomaly [rad]
%   EA - eccentric anomaly [rad]
%   N_versor - versor of node line vector
%   H - the angular momentum vector [km^2/s]
% ------------------------------------------------------------

function [a h e RA incl w TA EA N_versor H] = state_vector_2_orbit_elements(R, V)
	global mu_sun;
	eps = 1e-10;

	% R = transpose(R);
	% V = transpose(V);

	r = norm(R);
	v = norm(V);
	
	vr = dot(R,V)/r;
	
	H = cross(R,V);
	h = norm(H);
	
	%...Equation 4.7:
	incl = acos(H(3)/h);
	
	%...Equation 4.8:
	N = cross([0 0 1],H);
	n = norm(N);
	N_versor = transpose(N) / n;
	
	%...Equation 4.9:
	if n ~= 0
		RA = acos(N(1)/n);
		if N(2) < 0
			RA = 2*pi - RA;
		end
	else
		RA = 0;
	end

	%...Equation 4.10:
	E = 1/mu_sun*((v^2 - mu_sun/r)*R - r*vr*V);
	e = norm(E);

	%...Equation 4.12 (incorporating the case e = 0):
	if n ~= 0
		if e > eps
			w = acos(dot(N,E)/n/e);
			if E(3) < 0
				w = 2*pi - w;
			end
		else
			w = 0;
		end
	else
		w = 0;
	end

	%...Equation 4.13a (incorporating the case e = 0):
	if e > eps
		TA = acos(dot(E,R)/e/r);
		if vr < 0
			TA = 2*pi - TA;
		end
	else
		cp = cross(N,R);
		if cp(3) >= 0
			TA = acos(dot(N,R)/n/r);
		else
			TA = 2*pi - acos(dot(N,R)/n/r);
		end
	end

	%...Equation 2.61 (a < 0 for a hyperbola):
	a = h^2/mu_sun/(1 - e^2);
	% coe = [h e RA incl w TA a];

	%...Equation 3.10b:
	EA = 2 * atan( sqrt((1-e)/(1+e)) * tan(TA/2));