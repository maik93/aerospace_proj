% algoritm 3.1, Appendix D from 'Curtins - Orbital mechanics for engineering students'

% ------------------------------------------------------------
% This function uses Newton's method to solve Kepler's equation E - e*sin(E) = M for the eccentric anomaly, given the eccentricity and the mean anomaly.
%
% Inputs:
%   e - eccentricity
%   M - mean anomaly [rad]
%
% Outputs:
%   E - eccentric anomaly [rad]
% ------------------------------------------------------------
function E = kepler_E(e, M)
	error = 1.e-8;

	%...Select a starting value for E:
	if M < pi
		E = M + e/2;
	else
		E = M - e/2;
	end

	%...Iterate on Equation 3.14 until E is determined to within the error tolerance:
	ratio = 1;
	while abs(ratio) > error
		ratio = (E - e*sin(E) - M)/(1 - e*cos(E));
		E = E - ratio;
	end