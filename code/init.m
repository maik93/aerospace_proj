load('workspace.mat')

addpath(genpath('porkchop'))
addpath(genpath('curtins'))
addpath(genpath('graphics'))
addpath(genpath('graphics/rgb'))

global days2seconds seconds2days mass_sun mass_earth mass_venus mu_sun mu_earth mu_venus r_earth r_venus T_earth T_venus n_earth n_venus store_animation
% Private: soi_earth soi_venus R_earth R_venus

store_animation = false;

days2seconds = 60*60*24;
seconds2days = 1 / days2seconds;

% Masses [kg]
mass_sun = 1.9891e30;
mass_earth = 5.9736e24;
mass_venus = 4.8685e24;

% Gravitational parameters [km^3 / s^2]
mu_sun = 132712441933.0; % better than 6.67e-11 * 1e-9 * mass_sun;
mu_earth = 6.67e-11 * 1e-9 * mass_earth;
mu_venus = 6.67e-11 * 1e-9 * mass_venus;

% Mean planet radious [km]
r_earth = 6378;
r_venus = 6052;

% Mean orbit radious [km] (CHECK IF IT'S MEAN FOR REAL)
% R_earth = 149.6e6;
% R_venus = 108.2e6;

% Spheres Of Influence [km]
% soi_earth = R_earth * (mass_earth / mass_sun)^(2/5);
% soi_earth = 9.2462e+05;
% soi_venus = R_venus * (mass_venus / mass_sun)^(2/5);
% soi_venus = 6.1620e+05;

% Orbit sideral periods [days]
T_earth = 365.256;
T_venus = 224.701;

% Mean motions [rads/days]
n_earth = 2*pi / T_earth;
n_venus = 2*pi / T_venus;

incl_venus_orbit = 0.4265; % [rad]