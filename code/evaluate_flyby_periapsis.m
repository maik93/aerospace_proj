% evaluate_flyby_periapsis: compute the correct hyperbola's periapsis that generate a turn angle of the probe that will create an orbit with period equals to Venus' orbital period.

% ------------------------------------------------------------
% Inputs:
%   deltaV - escape velocity at the enter of planet's SOI (vector) [km/s]
%   JD_encounter_venus - Venus encounter Julian date
%
% Outputs:
%   choosen_r_periapsis - resulting periapsis [km]
%   choosen_T - resulting period [days]
% ------------------------------------------------------------

function [choosen_r_periapsis, choosen_T] = evaluate_flyby_periapsis(deltaV, JD_encounter_venus)
	global r_venus T_venus mass_sun mass_venus mu_sun mu_venus seconds2days

	deltaV = -deltaV;
	[R_venus_encounter, v_venus_encounter] = p2000(11, 2, JD_encounter_venus);
	soi_venus = norm(R_venus_encounter) * (mass_venus / mass_sun)^(2/5);

	choosen_r_periapsis = 0;
	for actual_r = r_venus:100:soi_venus
		a = - mu_venus / norm(deltaV)^2; % not heliocentric!
		beta_angle = acos(1 / ((a - actual_r) / a));
		delta = pi - 2 * beta_angle;
		theta_inf =  pi + (pi - delta) / 2;

		deltaV_xy = [deltaV(1); deltaV(2); 0];
		z_rotation_angle = acos(deltaV_xy(1) / norm(deltaV_xy));
		y_rotation_angle = - acos(dot(deltaV, deltaV_xy) / (norm(deltaV) * norm(deltaV_xy)));

		rotZYZ = matrix_rotation(z_rotation_angle, 'Z') *  matrix_rotation(y_rotation_angle, 'Y') * matrix_rotation(-theta_inf, 'Z');

		v_inf_exit_versor = rotZYZ * matrix_rotation(pi - beta_angle, 'Z') * [1; 0; 0];

		actual_deltaV = norm(deltaV) * v_inf_exit_versor;
		actual_v_heliocentric = actual_deltaV + v_venus_encounter;

		[actual_a, ~, ~, ~, ~, ~, ~, ~] = ...
			state_vector_2_orbit_elements(R_venus_encounter, actual_v_heliocentric);

		actual_T = 2*pi / sqrt(mu_sun) * actual_a^(3/2) * seconds2days;
		if abs(actual_T - T_venus) < .5
			choosen_r_periapsis = actual_r;
			choosen_T = actual_T;
			return
		end
	end

	choosen_r_periapsis = 0;
	choosen_T = 0;
