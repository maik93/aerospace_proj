% find_times_for_hohmann: compute wait time and transfer time for a Hohmann transfer

% ASSUMPTION: incl_earth = incl_venus
% ------------------------------------------------------------
% Inputs:
%   JD0 - initial Julian date
%
% Outputs:
%   t_wait - wait time necessary to begin the transfer [days]
%   t_transf - Hohmann transfer time [days]
% ------------------------------------------------------------

function [t_wait, t_transf] = find_times_for_hohmann(JD0)
	global mu_sun seconds2days n_earth n_venus

	% Synotic Period [days]
	% Tsyn = 2*pi / abs(n_venus - n_earth);

	% Initial phase between Earth and Venus (through Mean Anomalies)
	[r0, v0] = p2000(11, 3, JD0);
	[~, ~, ~, RA_earth, incl_earth, w_earth, ~, EA0_earth] = state_vector_2_orbit_elements(r0, v0);
	R_earth = norm(r0);
	[r0, v0] = p2000(11, 2, JD0);
	[~, ~, ~, RA_venus, incl_venus, w_venus, ~, EA0_venus] = state_vector_2_orbit_elements(r0, v0);
	R_venus = norm(r0);

	% Transfer time [days]
	t_transf = pi / sqrt(mu_sun) * ((R_earth + R_venus)/2)^(3/2) * seconds2days;

	starting_phase = (EA0_earth + RA_earth + w_earth) - (EA0_venus + RA_venus + w_venus);
	% Initial phase between Earth and Venus through True Anomalies will differ from this version of -0.0015 rads (-0.0859 deg)

	desired_phase = pi - n_earth * t_transf; % [rad]

	% Evaluate minimun wait time for Hohmann transfer [days]
	N = 0;
	t_wait = -1;
	while t_wait < 0 && N < 10
		t_wait = (desired_phase - starting_phase - 2*pi*N) / (n_earth - n_venus);
		N = N + 1;
	end
	if t_wait < 0
		fprintf('Unable to find admissible wait time!')
	end