% evaluate_tof: compute hyperbolic time of fly for the transit in some planet's SOI

% ------------------------------------------------------------
% Inputs:
%   a - semimajor axis [km]
%   e - eccentricity
%   soi - radius of planet's SOI [km]
%   mu_planet - gravitational parameter [km^3/s^2]
%
% Outputs:
%   tof - time of fly [sec]
% ------------------------------------------------------------

function [tof] = evaluate_tof(a, e, soi, mu_planet)
	a = abs(a);

	% TA = acos(a * (1 - e^2) / (e * soi)) - 1 / e;
	TA = acos((a * (1 - e^2) - soi) / (e * soi));
	% E = atanh(sqrt((e - 1) / (e + 1))) * tan(TA / 2);
	E = 2 * atan(sqrt((e - 1) / (e + 1)) * tan(TA / 2));
	MA = e * sinh(E) - E;

	tof = 2 * MA * sqrt(a^3 / mu_planet);