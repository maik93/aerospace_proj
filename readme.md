# Preliminary planning for an Earth-Venus roundtrip autonomous mission - *Aerospace Robotics*

![return trip](https://gitlab.com/maik93/aerospace_proj/raw/master/cover_image.png)

The objective is to design an interplanetary trajectory from Earth to Venus (with at least one interplanetary fly-by), and to use a Hohmann transfer to return to Earth.

Delivery date: February 19, 2018

## Full documentation
A comprehensive documentation of this work can be found on the [uploaded report](https://gitlab.com/maik93/aerospace_proj/raw/master/delivery/relazione_Michael_Mugnai.pdf), covering dynamic model description, classical controls realization, LQG, Hinf and Mu controllers seen both in theory than in practice.

## Presentation slides
Slides used in support of the exposition during the exam can be found [here](https://gitlab.com/maik93/aerospace_proj/raw/master/delivery/RAS_Michael_Mugnai.pptx).